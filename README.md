# Setup

## Prerequest

- [ripgrep](https://github.com/BurntSushi/ripgrep)

run

```cmd
mkdir -p ~/.config/nvim/
rm -rf ~/.config/nvim/
git clone <this repository uri> ~/.config/nvim
```
